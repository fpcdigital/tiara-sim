﻿using UnityEngine;
using MiniJSON;
using System.Collections;
using System.Collections.Generic;

//TODO: Add physics sim

partial class Gamelogic : MonoBehaviour
{
	public static Gamelogic instance;
	public MotionController motionController;
	
//	private int numPlayers = 1;//not yet used
	public Jukebox juke;
	private int stateStep; //for internal progress tracking - resuable, fills the role of _hasStarted.
	
	public static int currentLevel = 0;

	private static Dictionary<string,string>[] audioSequenceLevels;

	public static Dictionary<string, string> audioSequences
	{
	get{
			Dictionary<string, string> returnSequences = new Dictionary<string, string>();
			for (int i = 0; i <= currentLevel; i++){
			
				foreach (string key in audioSequenceLevels[i].Keys){
					returnSequences.Add(key,audioSequenceLevels[i][key]);
				}
			}
			return returnSequences;
		}
	}
//	public static Dictionary<string, int[][]> moveSequences;
//	public static Dictionary<string, Vector3[]> spikeSequences;
//	public static Dictionary<string, int[]> twirlSequences;
	
	private static Dictionary<string, moveStruct>[] moveSequenceLevels;
	
	public static Dictionary<string, moveStruct> moveSequences{
		get{
			Dictionary<string, moveStruct> returnSequences = new Dictionary<string, moveStruct>() ;
			for (int i = 0; i <= currentLevel; i++){
				
				foreach (string key in moveSequenceLevels[i].Keys){
					returnSequences.Add(key,moveSequenceLevels[i][key]);
				}
			}
			return returnSequences;
		}
	}
	
	public struct moveStruct
	{
	
		public moveStruct (string myType, string myDir = "", int myNum = 1)
		{
			type = myType;
			dir = myDir;
			num = myNum;
		}
		
		public string type;
		public string dir;
		public int num;
	}

	public enum ButtonCode
	{
		None,
		Start,
		Select,
		Station
	}

	public enum GameMode
	{
		MemoryInMotion,
		RedGemGreenGem,
		TimeToMove,
		Instrument,//RapidDanceyTimes until told otherwise
		FairyGodmotherSays
	}

//	private Queue<int> recentDanceMoves;
	private ButtonCode _buttonCode;
	private int[] scores;
//	private float lastInputStamp;
	public int numPlayers = 1;

public void Awake(){
		if (motionController==null){
			motionController = GameObject.FindObjectOfType<MotionController>();
		}
		if (motionController==null){
			motionController = new GameObject("MotionController",typeof(MotionController)).GetComponent<MotionController>();
		}
		if (juke==null){
			juke = GameObject.FindObjectOfType<Jukebox>();
		}
		if (juke==null){
			juke = new GameObject("MotionController",typeof(Jukebox)).GetComponent<Jukebox>();
		}
}

	public void Start ()
	{
		print ("start");
		instance = this;
//		lastInputStamp = Time.time;
//		recentDanceMoves = new Queue<int> ();
		juke.playBGM = false;
		BuildSequences ();
//		numPlayers = 1;
		StartCoroutine (MainLoop ());
	}

	public IEnumerator MainLoop ()
	{
		//level select
		//wait for button //if multiplayer 
		//if select, change game mode:
		//if start, go:
		InterpretAndQueue ("INTRO1");
		print ("main loop");
//		yield return StartCoroutine(playersSelect());
		while (true) {
			//yield return StartCoroutine(MemoryInMotion());
			yield return StartCoroutine (modeSelect ());
			yield return waitForSpeech();
			//List out each player's points (assuming all games are points), go to next player(?)
			//option to restart or select a different game
			
			//current player and game mode are global state, pass into game select coroutine, game mode coroutine, read out coroutine, restart coroutine
		}//on-screen buttons;
	}
	
	public GameMode currentMode = GameMode.MemoryInMotion;
	
	/*//unused
	public IEnumerator playersSelect ()
	{
		InterpretAndQueue ("INTRO2a");
		while (_buttonCode!=ButtonCode.Start) {//this is compact but weird
			_buttonCode = ButtonCode.None;
			InterpretAndQueue ("N" + numPlayers + "+INTRO2" + (numPlayers == 1 ? "b" : "c"));
			yield return StartCoroutine (waitForButton ());
			if (_buttonCode == ButtonCode.Select) {
				numPlayers = (numPlayers % 4) + 1;
			}
		}
	}*/
	
	public IEnumerator modeSelect ()
	{
		InterpretAndQueue ("INTRO3");
		yield return StartCoroutine (modeSelectQuiet ());
	}

	public IEnumerator modeSelectQuiet ()
	{
		switch (currentMode) {
		case GameMode.MemoryInMotion:
			juke.StopPlaying();
			InterpretAndQueue ("RS1a");
			yield return StartCoroutine (waitForButton ());
			juke.StopPlaying ();
			if (_buttonCode == ButtonCode.Start) {
				_buttonCode = ButtonCode.None;
				yield return StartCoroutine (DanceParty ());
			} else if (_buttonCode == ButtonCode.Select) {
				_buttonCode = ButtonCode.None;
				currentMode = GameMode.Instrument;
				yield return StartCoroutine (modeSelectQuiet ());//Tail Recursion. I'M SO SORRY PLEASE FORGIVE ME. Had to avoid post-gamemode coroutine returning to here or another mode switch statement to start the related coroutine.
			}
			break;
//		case GameMode.FairyGodmotherSays:
//			InterpretAndQueue ("RS2a");
//			yield return StartCoroutine (waitForButton ());
//			juke.StopPlaying ();
//			if (_buttonCode == ButtonCode.Start) {
//				_buttonCode = ButtonCode.None;
//				yield return StartCoroutine (FairyGodmotherSays ());
//			} else if (_buttonCode == ButtonCode.Select) {
//				_buttonCode = ButtonCode.None;
//				//				currentMode = GameMode.TimeToMove;
//				currentMode = GameMode.Instrument;
//				yield return StartCoroutine (modeSelectQuiet ());//Tail Recursion. I'M SO SORRY PLEASE FORGIVE ME.
//				}
//				break;
//		case GameMode.RedGemGreenGem:
//			InterpretAndQueue ("RS3a");
//			yield return StartCoroutine (waitForButton ());
//			if (_buttonCode == ButtonCode.Start) {
//				_buttonCode = ButtonCode.None;
//				yield return StartCoroutine (RedGemGreenGem ());
//			} else if (_buttonCode == ButtonCode.Select) {
//				_buttonCode = ButtonCode.None;
//				currentMode = GameMode.TimeToMove;//skip storyOnStage
//				yield return StartCoroutine (modeSelectQuiet ());//Tail Recursion. I'M SO SORRY PLEASE FORGIVE ME.
//			}
//			break;
		case GameMode.Instrument://this mode is a seeeeecret
			juke.StopPlaying();
			InterpretAndQueue ("RS3a");
			yield return StartCoroutine (waitForButton ());
			if (_buttonCode == ButtonCode.Start) {
				_buttonCode = ButtonCode.None;
				yield return StartCoroutine (RLGL ());
			} else if (_buttonCode == ButtonCode.Select) {
				_buttonCode = ButtonCode.None;
				currentMode = GameMode.MemoryInMotion;
				yield return StartCoroutine (modeSelectQuiet ());//Tail Recursion. I'M SO SORRY PLEASE FORGIVE ME.
			}
			break;
//		case GameMode.TimeToMove:
//			InterpretAndQueue("RS4a");
//			yield return StartCoroutine (waitForButton ());
//			juke.StopPlaying();
//			if (_buttonCode == ButtonCode.Start) {
//				_buttonCode = ButtonCode.None;
//				yield return StartCoroutine (TimeToMove ());
//			} else if (_buttonCode == ButtonCode.Select) {
//				_buttonCode = ButtonCode.None;
//				currentMode = GameMode.MemoryInMotion;
//				yield return StartCoroutine (modeSelectQuiet ());//Tail Recursion. I'M SO SORRY PLEASE FORGIVE ME.
//			}
//			break;
			default:
			currentMode = GameMode.MemoryInMotion;
			yield return StartCoroutine (modeSelectQuiet ());
				break;
		}
	}
	
	public IEnumerator nodButtons(){
	
		bool[] report = new bool[3];
		yield return StartCoroutine(motionController.detectNod(report));
		if(report[0]){
		_buttonCode = ButtonCode.Select;}
		else if (report[1]){
		_buttonCode = ButtonCode.Start;}
	
	}
	
	public Coroutine WaitForMove (string moveName, bool[] successFlag, float timeLimit)
	{
		if(moveName == "none"){
			return StartCoroutine(WaitForMove(new moveStruct("none"),successFlag,timeLimit));
		} else {
//		if(moveName.Substring(0,1).Equals("T")){motionController.dingDelay=1.5f;}
		return StartCoroutine(WaitForMove (moveSequences [moveName], successFlag, timeLimit));
		}
	}

	public IEnumerator WaitForMove (moveStruct move, bool[] successFlag, float TimeLimit)
	{
	
		float TimeStamp = Time.time;
	
		float[] success = new float[1];
	
		IEnumerator controllerRoutine;// = (motionController.waitForDance (TimeLimit, timeOutFlag));
		IEnumerator failRoutine1;
		IEnumerator failRoutine2;
		switch (move.type) {
		case "hop":
			controllerRoutine = motionController.EvaluateHop (move.dir, move.num, success);
			failRoutine1 = motionController.evaluateLeanFail("backward", new float[1]);
			failRoutine2 = motionController.evaluateTwirl(move.dir, move.num, new float[1]);
			break;
		case "twirl":
			controllerRoutine = motionController.evaluateTwirl (move.dir, move.num, success);
			failRoutine1 = motionController.EvaluateHop(move.dir, move.num, new float[1]);
			failRoutine2 = motionController.evaluateLeanFail("backward", new float[1]);
			break;
		case "lean":
			controllerRoutine = motionController.evaluateLean (move.dir, success);
			failRoutine1 = motionController.EvaluateHop(move.dir,1, new float[1]);
			failRoutine2 = motionController.evaluateTwirl(move.dir, 1, new float[1]);
			break;
		case "none":
			controllerRoutine = motionController.evaluateLean (move.dir, new float[1]);
			failRoutine1 = motionController.EvaluateHop(move.dir,1, new float[1]);
			failRoutine2 = motionController.evaluateTwirl(move.dir, 1, new float[1]);
			break;
		default:
			throw new UnityException("Not a move type");
		}
		
		while (controllerRoutine.MoveNext() && failRoutine1.MoveNext() && failRoutine2.MoveNext() && Time.time < TimeStamp + TimeLimit){
			yield return null;
		}
//		while (success[0]<1f && controllerRoutine.MoveNext()) {
//			yield return null;
//		}
		successFlag [0] = (success [0] == 1f) || (Time.time >= TimeStamp + TimeLimit && move.type == "none");
	}
	
	

	int[] ShuffleIntArray (int[] source) //don't know if this will disrespect source list. It's possible that it will, but that doesn't hurt anything in this game.
	{
		for (int i = source.Length - 1; i >= 0; i--) {
			var j = Random.Range (0, i);
			var temp = source [j];
			source [j] = source [i];
			source [i] = temp;
		}
		return source;
	}
	
	public IEnumerator waitForSpeech ()
	{
		while (juke.isUnquiet()) {
			yield return null;
		}
	}
	
	public IEnumerator waitForButton ()
	{
//		StartCoroutine (nodButtons());
		while (_buttonCode == ButtonCode.None) {
			yield return null;
		}
	}

	public void StartPressed ()
	{
		_buttonCode = ButtonCode.Start;
//		lastInputStamp = Time.time;
	}

	public void SelectPressed ()
	{
		_buttonCode = ButtonCode.Select;
//		lastInputStamp = Time.time;
	}

	void StationPressed (int stationNum)
	{
		_buttonCode = ButtonCode.Station;
//		lastInputStamp = Time.time;
	}

	void BuildSequences ()
	{
		audioSequenceLevels = new Dictionary<string, string>[3];
		moveSequenceLevels = new Dictionary<string, moveStruct>[3];
		var dict = new Dictionary<string, object> ();
		dict = (Dictionary<string, object>)Json.Deserialize (Resources.Load<TextAsset> ("DanceMoves").text);
//		dict = (Dictionary<string, object>)Json.Deserialize (Resources.Load<TextAsset> ("DanceMovesSimple").text);
		foreach (var key in dict.Keys) {
			Dictionary<string,object> obj = (Dictionary<string,object>)dict [key];
			int level = 0;
			if (obj.ContainsKey("level")){
				level = ((int)(long)(obj["level"]))-1;
			}
			if(audioSequenceLevels.Length <= level){
				Dictionary<string, string>[] temp = new Dictionary<string, string>[level+1];
				audioSequenceLevels.CopyTo(temp,0);
				audioSequenceLevels = temp;
			}
			if( audioSequenceLevels[level]==null){
				audioSequenceLevels[level] = new Dictionary<string, string>();
			}
			audioSequenceLevels[level].Add (key, obj ["VO"].ToString ());
			
			string type = (string)obj ["type"];
			string dir = "";
			if (obj.ContainsKey ("dir")) {
				dir = (string)obj ["dir"];
			}
			int num = 1;
			if (obj.ContainsKey ("num")) {
				
				num = (int)(long)obj ["num"];
			}
			if(moveSequenceLevels.Length <= level){
				Dictionary<string, moveStruct>[] temp = new Dictionary<string, moveStruct>[level+1];
				moveSequenceLevels.CopyTo(temp,0);
				moveSequenceLevels = temp;
			}
			if(moveSequenceLevels[level]==null){
				moveSequenceLevels[level] = new Dictionary<string, moveStruct>();
			}
			moveSequenceLevels[level].Add (key, new moveStruct (type, dir, num));
		}
	}
	
	public IEnumerator sayPoints(int points){
		juke.QueueOutput("CORRECT3+MUSIC4+MM3+SCORE1+N" + points + "+SCORE2");
		yield return StartCoroutine (waitForSpeech ());
	}
	
	public IEnumerator sayPointsAndDie (int points)
	{
		yield return StartCoroutine(sayPoints(points))	;
		juke.QueueOutput("pin_dropping");
		yield return waitForSpeech();
		Application.Quit ();
		Debug.Log ("Quitting");
	}
	
	void InterpretAndQueue (string toQueue)
	{
		var QueueList = toQueue.Split ('+');
		for (int i = 0; i < QueueList.Length; i++) {
			if (audioSequences.ContainsKey (QueueList [i])) {
				InterpretAndQueue (audioSequences [QueueList [i]]);
			} else {
				juke.QueueOutput (QueueList [i]);
			}
		}
	}
	//#region conveniences
	public static IEnumerable<string> RandomKeys (IDictionary<string, string> dict)
	{
		string[] keysList = new string[dict.Keys.Count];
		dict.Keys.CopyTo (keysList, 0);
		int size = dict.Count;
		while (true) {
			yield return keysList [Random.Range (0, size)];
		}
	}
	//#endregion
	

	
	public void ResetGame ()
	{
		Application.LoadLevel (Application.loadedLevel);
	}
}
