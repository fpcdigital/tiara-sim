﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

partial class Gamelogic
{

	private List<string> simonStack;

	public IEnumerator MemoryInMotion ()
	{
		int points = 0;
		juke.QueueOutput ("RS1b+RS6");
		simonStack = new List<string> ();
		yield return null;
		IEnumerator<string> keyDropper = new KeyDropper();
//		keyDropper.MoveNext();
//		simonStack.Add(keyDropper.Current);
		bool lost = false;
		while (!lost) {
			if(simonStack.Count > 14){currentLevel = 2;} else if(simonStack.Count >9){currentLevel = 1;} else {currentLevel = 0;}
			
			string nextMove;
			do {
				keyDropper.MoveNext ();
				nextMove = keyDropper.Current;
			} while (simonStack.Count > 0 && nextMove.Substring(0, 1).Equals(simonStack[simonStack.Count - 1].Substring(0, 1)));
			simonStack.Add (nextMove);
			int numTries = 0;
			lost = true;
			points+=3;
			while (numTries < 3 && lost){
			InterpretAndQueue ("RS7");
			InterpretAndQueue (simonStack [0]);
			for (int i = 1; i < simonStack.Count; i++) {
				InterpretAndQueue ("CON1");
				InterpretAndQueue (simonStack [i]);
			}
			InterpretAndQueue ("RS8a+RS8b");
			yield return StartCoroutine (waitForSpeech ());
				juke.SetBGM("background");
			for (int i = 0; i < simonStack.Count; i++) {
				
				nextMove = simonStack [i];
				print ("waiting for " + nextMove);
				
				
				bool[] success = new bool[2];
				
				yield return WaitForMove (nextMove, success, 15f);
//				IEnumerator talkRoutine = waitForSpeech();
////				IEnumerator talkRoutine = waitForSeconds(1f);
//				while(moveRoutine.MoveNext()||talkRoutine.MoveNext()){
//					yield return null;
//				}
				
				
				if (success [0]) {
					juke.PlayOutput ("correct");
					lost = false;
					yield return new WaitForSeconds (1f);
				} else {
					points--;
					juke.PlayOutput ("incorrect");
						juke.playBGM = false;
					lost = true;
					break;
				}

			}
			
			if (!lost) {
				juke.PlayOutput ("CORRECT" + Random.Range (1, 5).ToString ());
				points++;
			} else {
					if(numTries == 0){
						juke.playBGM = false;
						InterpretAndQueue("FAIL1");
					} else if(numTries == 1){
						juke.playBGM = false;
						InterpretAndQueue("FAIL2");
					}
					
					points--;
					numTries++;
				}
			}
			//new round
		}
		InterpretAndQueue ("MUSIC4+MM3+GE1+N" + points + "+GE2+REPEAT1");
		//points and restart
	}

	IEnumerator waitForSeconds (float numSeconds)
	{
		yield return new WaitForSeconds (numSeconds);
	}

}
