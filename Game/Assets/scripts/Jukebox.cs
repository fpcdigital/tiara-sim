﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Jukebox : MonoBehaviour
{
	public AudioSource source;
	public AudioSource BGsource;
	public Queue<string> queue = new Queue<string>();
	public float stamp = 0;
	public float interruptStamp = 0;
	public string interruptString;
	public bool isPlayingBG;
	public bool queueIsLocked = false;
	public bool playBGM;
	public bool loopBGM{
		set{BGsource.loop = value;}
	}

	void Start()
	{
		source = GetComponent<AudioSource>();
		stamp = Time.time - 1;
		//        interruptStamp = 10000f;
	}

	void Update()
	{
		//        if (isPlayingBG)
		//        {
		//            interruptStamp -= Time.deltaTime;
		//            if (interruptStamp <= 0f)
		//            {
		//                PlayOutput(interruptString);
		//                interruptStamp = 10000f;
		//            }
		//        }
		if (!(source.isPlaying || stamp > Time.time))
		{
			if (queue.Count > 0)
			{
				isPlayingBG = false;
				string nextClipString = queue.Dequeue();
				AudioClip clip = null;
				if (nextClipString != "@")
				{
					clip = Resources.Load<AudioClip>(nextClipString.Replace("\r", ""));
					Debug.Log(nextClipString.Replace("\r", ""));
				}

				if (nextClipString == "^")
				{
					Application.Quit();
				}

				if (clip == null)
				{
					stamp = Time.time + .25f;
				}

				source.clip = clip;

				if (source.clip != null)
				{
					print(nextClipString);
					source.loop = false;
					source.Play();
				}
			}
			else
			{
				queueIsLocked = false;
				isPlayingBG = true;
			}
		}
		if (!queueIsLocked && playBGM && !BGsource.isPlaying)
		{
//			BGsource.clip = Resources.Load<AudioClip>("background"); //background music loop
			BGsource.loop = true;
			BGsource.Play();
		}
		else if (!playBGM && BGsource.isPlaying)
		{
			BGsource.Stop();
		}
	}

	public void LockQueue()
	{
		if (!queueIsLocked)
		{
			queueIsLocked = true;
		}
	}
	
	int step = 0;
	void mark(){
//		Debug.Log(step++);
	}
	
	private List<AudioSource> oneShotSources;
	
	AudioSource playSource;
	
	public void playOneShot(string clipName, float pitch = 1f){
		step = 0;
//		if(oneShotSources == null){
//			oneShotSources = new List<AudioSource>();
//		}
//		AudioSource playSource = null;

//		foreach(AudioSource source in oneShotSources){
//			if (!source.isPlaying && source == null){
//				playSource = source;
//			}
//		}
//		if(playSource == null){
//			playSource = (gameObject.AddComponent<AudioSource>());
//			oneShotSources.Add(playSource);
//		}

		if(playSource == null){
			playSource = (gameObject.AddComponent<AudioSource>());
		}
		playSource.Stop();
		AudioClip playClip = Resources.Load<AudioClip>(clipName);
		if(playClip == null){
			playClip = Resources.Load<AudioClip>("failBuzzer");
		}

		playSource.clip = playClip;
		playSource.pitch = pitch;

		if(playSource.clip!=null){
			playSource.Play();
		}
	}
	
	public void SetBGM(string clipname, bool play = true){
		BGsource.Stop();
		BGsource.clip = Resources.Load<AudioClip>(clipname);//fail loudly
		if (BGsource.clip == null){
			BGsource.clip = Resources.Load<AudioClip>("failBuzzer");
			BGsource.Play();
		}
		if(play){
			BGsource.Play();
		}
	}

	public void PlayOutput(string s)
	{
		StopPlaying ();
		if (s.Length > 0)
		{
			var toQueue = s.Split('+');
			foreach (string psi in toQueue)
			{
				int num;
				if (psi.Substring(0, 1) == "N" && int.TryParse(psi.Substring(1),out num))
				{
					if (num == 0)
					{
						queue.Enqueue("N0");
					}
					else
					{
						if (num > 99)
						{
							queue.Enqueue("N" + (num / 100) + "00");
							num = num % 100;
						}
						if (num > 20)
						{
							queue.Enqueue("N" + (num / 10) + "0");
							num = num % 10;
						}
						queue.Enqueue("N" + (num));
					}
				}
				else
				{
					queue.Enqueue(psi);
				}
			}
		}
	}

	public bool isUnquiet()
	{
		return queue.Count > 0 || source.isPlaying;
	}

	public void QueueOutput(string s)
	{
		if (!queueIsLocked)
		{
			if (isPlayingBG)
			{
				source.Stop();
			}
			var toQueue = s.Split('+');
			foreach (string psi in toQueue)
			{
				int num;
				if (psi.Substring(0, 1) == "N" && int.TryParse(psi.Substring(1), out num))
				{
					if (num == 0)
					{
						queue.Enqueue("N0");
					}
					else
					{
						if (num > 99)
						{
							queue.Enqueue("N" + (num / 100) + "00");
							num = num % 100;
						}
						if (num > 20)
						{
							queue.Enqueue("N" + (num / 10) + "0");
							num = num % 10;
						}
						queue.Enqueue("N" + (num));
					}
				}
				else
				{
					queue.Enqueue(psi);
				}
			}
		}
	}

	//public void QueueInterrupt(string s)
	//{
	//    interruptStamp = 3f;
	//    interruptString = s;
	//}

	public void StopPlaying ()
	{
		queueIsLocked = false;
		source.Stop ();
		queue = new Queue<string> ();
	}
}
