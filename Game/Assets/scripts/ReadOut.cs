using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ReadOut : MonoBehaviour
{
	public Text text;
	public Text text2;
	//public Image image;
	public RawImage rawImage;
	public Material graphMaterial;
	private Vector3 gyroMax;
	private Vector3 gyroMin;
	private Vector3 accMax;
	private Vector3 accMin;
	private Vector3 currentVelocity;
	private Vector4 scaleMax;
//	private Vector3 dragAverage;
	private Vector3 lastAccel;
	private Vector3[] recentChanges;
	private Vector3[] recentMotion;

	private Vector3 baseVector{ get { return Vector3.down; } }

	private int lastSecond;
	private Texture2D graphTexture;
	public int textureSize;
	private Vector3 baseGravitZ;
	private Vector3 velocity;
	private Vector3 posLol;

	void Start ()
	{
		Input.gyro.enabled = true;
		graphTexture = new Texture2D (textureSize, textureSize);
		scaleMax = Vector3.one;
		lastAccel = Vector3.one;
		recentChanges = new Vector3[30];
		recentMotion = new Vector3[30];
		lastSecond = System.DateTime.Now.Second;
//		baseVector = Input.acceleration;
		ResetRamps ();
		toDing = new System.Collections.Generic.Queue<float> ();
		Input.gyro.enabled = true;
		baseGravitZ = Vector3.down;
		twirlRamps = new int[4];
		twirlCounts = new int[2];
		currentVelocity = Vector3.zero;
		dinged = new HashSet<int> ();
	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Space)) {
			MakeMark ();
		
		}
//		currentVelocity += (Input.acceleration  - Input.gyro.gravity) * Time.deltaTime;
//		currentVelocity += (Input.acceleration ) * Time.deltaTime;
		Vector3 cacheAccel = CorrectForCalibration (Input.acceleration - Input.gyro.gravity);
		currentVelocity += cacheAccel * Time.deltaTime / 2f + lastAccel * Time.deltaTime / 2f;
		posLol += currentVelocity;
//		currentVelocity += cacheAccel * Time.deltaTime;
		
		var diffAccel = (cacheAccel - lastAccel);
		recentChanges [0] += (new Vector3 (Mathf.Abs (diffAccel.x), Mathf.Abs (diffAccel.y), Mathf.Abs (diffAccel.z))) * Time.deltaTime * 1000f;
		recentMotion [0] += (cacheAccel) * Time.deltaTime * 1000f;
		
		lastAccel = cacheAccel;
		if (lastSecond != (System.DateTime.Now.Millisecond / 100)) {
			TallyRamps (recentMotion [0]);
//			text2.text = findExtremes(recentMotion);
			if (recentRamps != Vector3.zero) {
//				text.text = (recentRamps.x * 2f).ToString ();
			}
			Vector3 vals = sum (recentMotion);
			Vector3 mags = sum (recentChanges);
//			text.text = mags.ToString();
//			text2.text = mags.sqrMagnitude.ToString();
			//if (sum (recentChanges, 0, 5).sqrMagnitude < 250f) {
			if (mags.x > 50f) {
				if (vals.x > 10) {
					Ding (0);
				} else if (vals.x < -10f) {
					Ding (1);
				} else {
					Ding (2);
				}
			}
			if (mags.y > 50f) {
				if (vals.y > 10) {
					Ding (3);
				} else if (vals.y < -10f) {
					Ding (4);
				} else {
					Ding (5);
				}
			}
			if (mags.z > 50f) {
				if (vals.z > 10) {
					Ding (6);
				} else if (vals.z < -10f) {
					Ding (7);
				} else {
					Ding (8);
				}
			}
			
			if (mags.sqrMagnitude < 1000f) {
				currentVelocity = Vector3.zero;
				posLol = Vector3.zero;
				baseGravitZ = (baseGravitZ * 5f + Input.gyro.gravity) / 6f;
				MakeMark ();
			}
			
//			Vector3 leanAngle = Input.gyro.gravity - baseGravitZ;
//			if(leanAngle.magnitude > .2f){
//				if(leanAngle.z < -.3f){//fwd
//					leanDing(0);
//				} else if (leanAngle.z > .3f){//bwd
//					leanDing(2);
//				}
//				if(leanAngle.x > .3f){//r
//					leanDing(1);
//				} else if(leanAngle.x < -.3f){
//					leanDing(3);
//				}
//			}
			
			//if (dinging)
			//{
			//	//recentChanges = new Vector3[30];
			//	//recentMotion = new Vector3[30];
			//	//lastSecond = System.DateTime.Now.Second;
			//	//baseVector = Input.acceleration;
			//	ResetRamps();
			//}
			//}
//			if (mags.sqrMagnitude < 100f) {
//				baseVector = cacheAccel;


//			}
			for (int i = recentChanges.Length-2; i>=0; i--) {
				recentChanges [i + 1] = recentChanges [i];
			}
			recentChanges [0] = Vector3.zero;
			
			for (int i = recentMotion.Length-2; i>=0; i--) {
				recentMotion [i + 1] = recentMotion [i];
			}
			recentMotion [0] = Vector3.zero;
			
			lastSecond = (System.DateTime.Now.Millisecond / 100);
		}
		
//		text.text = joinFloatArray(recentChanges);
		//text.text = sum (recentChanges, 0, 5).sqrMagnitude.ToString("F1") +"  " + sum (recentChanges).ToString ("F4") + " " + sum (recentMotion).ToString ("F4");

//		dragAverage += (currentVelocity - dragAverage).normalized * .001f;
//		if ((currentVelocity - dragAverage).magnitude < .05f)
//		{
////			currentVelocity *= .95f;
//			//currentVelocity = Vector3.zero;
//		}
//		text.text = currentVelocity + "  " + dragAverage + "  " + ((currentVelocity - dragAverage).magnitude) + "  " + ((currentVelocity - dragAverage).magnitude < .1f);


//		Vector3 leanerAngle = Input.gyro.gravity - baseGravitZ;
		
//		var toDisplay = cacheAccel;//CorrectForCalibration(Input.acceleration - Input.gyro.gravity);
//		var att = Input.gyro.attitude;

		var att = Input.gyro.attitude;
		//			#if UNITY_ANDROID
		//		var faceAngle = att.x+att.y+att.z+att.w;
		//			#elif UNITY_IOS
		var upAngle = Mathf.Atan2 (att.y - att.x, att.z - att.w); //new Vector2(((((Mathf.Atan2(Input.gyro.attitude.x+Input.gyro.attitude.w,Input.gyro.attitude.y+Input.gyro.attitude.z)/Mathf.PI)*2f)*2f+4f)%4-2f),scaleMax.y / scaleMax.x);//new Vector3(0,Input.gyro.attitude.z - Input.gyro.attitude.y,Input.gyro.attitude.x - Input.gyro.attitude.w);		
		
		var toDisplay = new Vector4 (upAngle, upAngle, upAngle, 0f);
		
//		  new Vector3(
//			Mathf.Atan2 (att.z+att.x,att.y+att.w),
//			Mathf.Atan2 (att.z+att.w,att.x+att.y),
//			Mathf.Atan2 (att.z+att.y,att.x+att.w)
//		);
		
//		scaleMax *= .9999f;

		text2.text = (scaleMax.x * 2f).ToString ();
		text.text = upAngle.ToString();

		checkTwirlBlock (toDisplay.x);

		if (Mathf.Abs (toDisplay.x) > scaleMax.x) {
			scaleMax.x = Mathf.Abs (toDisplay.x) * 1.1f;
		}
		if (Mathf.Abs (toDisplay.y) > scaleMax.y) {
			scaleMax.y = Mathf.Abs (toDisplay.y) * 1.1f;
		}
		if (Mathf.Abs (toDisplay.z) > scaleMax.z) {
			scaleMax.z = Mathf.Abs (toDisplay.z) * 1.1f;
		}
		if (Mathf.Abs (toDisplay.w) > scaleMax.w) {
			scaleMax.w = Mathf.Abs (toDisplay.w);
		}

		var pixels = graphTexture.GetPixels ();
		Color[] newPixels = new Color[pixels.Length];

		for (int i = 0; i < pixels.Length; ++i) {
			if (i % textureSize != 0) {
				newPixels [i - 1] = pixels [i];
			}
			newPixels [i] = Color.black;
		}
		var newPoint = (int)((-toDisplay.x / scaleMax.x) * textureSize + textureSize) / 2;
		if (newPoint >= textureSize || newPoint < 0) {
			newPoint = textureSize - 1;
		}
		newPixels [newPoint * textureSize + (textureSize - 1)] += Color.red;

		newPoint = (int)((-toDisplay.y / scaleMax.y) * textureSize + textureSize) / 2;
		if (newPoint >= textureSize || newPoint < 0) {
			newPoint = textureSize - 1;
		}
		newPixels [newPoint * textureSize + (textureSize - 1)] += Color.yellow;

		newPoint = (int)((-toDisplay.z / scaleMax.z) * textureSize + textureSize) / 2;
		if (newPoint >= textureSize || newPoint < 0) {
			newPoint = textureSize - 1;
		}
		newPixels [newPoint * textureSize + (textureSize - 1)] += Color.green;
       
		newPoint = (int)((-toDisplay.w / scaleMax.w) * textureSize + textureSize) / 2;
		if (newPoint >= textureSize || newPoint < 0) {
			newPoint = textureSize - 1;
		}
		newPixels [newPoint * textureSize + (textureSize - 1)] = Color.blue;




		graphTexture.SetPixels (newPixels);
		graphMaterial.mainTexture = graphTexture;
		rawImage.material = graphMaterial;
		graphTexture.Apply ();
		rawImage.texture = graphTexture;
	}
	
	public void MakeMark ()
	{
		if (gameObject.activeInHierarchy) {
			var pixels = graphTexture.GetPixels ();
			for (int i = 0; i < pixels.Length; ++i) {
				if (i % textureSize == textureSize - 1 && pixels [i] == Color.black) {
					pixels [i] = Color.gray;
				}
			
			}
			graphTexture.SetPixels (pixels);
			graphMaterial.mainTexture = graphTexture;
			rawImage.material = graphMaterial;
			graphTexture.Apply ();
			rawImage.texture = graphTexture;
		
			currentVelocity = Vector3.zero;
			posLol = Vector2.zero;
		}
	}

	public int[] twirlCounts;
	public int[] twirlRamps;

	private void checkTwirlBlock (float currentVal)
	{
		int newBlock = (int)currentVal;
		if (newBlock != twirlRamps [twirlRamps.Length - 1]) {
			for (int i = 1; i < twirlRamps.Length; i++) {
				twirlRamps [i - 1] = twirlRamps [i];
			}
			twirlRamps [twirlRamps.Length - 1] = newBlock;
			if (twirlRamps.SequenceEqual (new int[]{-1,0,1,-1}) ||
				twirlRamps.SequenceEqual (new int[]{0,1,-1,0}) ||
				twirlRamps.SequenceEqual (new int[]{1,-1,0,1})
			   ) {
				twirlCounts [0]++;
				twirlRamps = new int[4];
			} else if (twirlRamps.SequenceEqual (new int[]{-1,1,0,-1}) ||
				twirlRamps.SequenceEqual (new int[]{0,-1,1,0}) ||
				twirlRamps.SequenceEqual (new int[]{1,0,-1,1})) {
				twirlCounts [1]++;
				twirlRamps = new int[4];//fill with last element?
			}
//			text.text = join (twirlCounts);
//			text2.text = join (twirlRamps);
		}
	}

	public static string join (int[] toJoin)
	{
		return string.Join (",", toJoin.Select (x => x.ToString ()).ToArray ());
	}

	public Vector3 CorrectForCalibration (Vector3 source)
	{
		return Quaternion.FromToRotation (Input.gyro.gravity, Vector3.down) * source;
	}

	static float sum (float[] toSum)
	{
		float result = 0f;
		for (int i = 0; i < toSum.Length; i++) {
			result += toSum [i];
		}
		return result;
	}
	
	static Vector3 sum (Vector3[] toSum)
	{
		Vector3 result = Vector3.zero;
		for (int i = 0; i < toSum.Length; i++) {
			result += toSum [i];
		}
		return result;
	}
	
	static float sum (float[] toSum, int offset, int length)
	{
//		if(offset+length)
		float result = 0f;
		for (int i = offset; i < length; i++) {
			result += toSum [i];
		}
		return result;
	}
	
	static Vector3 sum (Vector3[] toSum, int offset, int length)
	{
		Vector3 result = Vector3.zero;
		for (int i = offset; i < length; i++) {
			result += toSum [i];
		}
		return result;
	}

	/*static dynamic sum(dynamic[] toSum)
	{
		dynamic result;
		if (toSum.Length > 0)
		{
			result = toSum[0];
			for (int i = 1; i < toSum.Length; i++)
			{
				result += toSum[i];
			}
			return result;
		}
		throw new UnityException();
	}*/

	static string joinArray (float[] toJoin)
	{
		if (toJoin.Length == 0) {
			return "";
		}
		string result = toJoin [0].ToString ();
		for (int i = 1; i < toJoin.Length; i++) {
			result += "," + toJoin [i].ToString ();
		}
		return result;
	}
	
	static string joinArray (string[] toJoin)
	{
		if (toJoin.Length == 0) {
			return "";
		}
		string result = toJoin [0].ToString ();
		for (int i = 1; i < toJoin.Length; i++) {
			result += "," + toJoin [i].ToString ();
		}
		return result;
	}
	
	static string joinArray (Vector3[] toJoin)
	{
		if (toJoin.Length == 0) {
			return "";
		}
		string result = toJoin [0].ToString ();
		for (int i = 1; i < toJoin.Length; i++) {
			result += "," + toJoin [i].ToString ();
		}
		return result;
	}

	static string findExtremes (Vector3[] toSearch)
	{
		Vector3 maxes = Vector3.zero;
		Vector3 mins = Vector3.zero;
		foreach (Vector3 v in toSearch) {
			if (v.x > maxes.x) {
				maxes.x = v.x;
			}
			if (v.y > maxes.y) {
				maxes.y = v.y;
			}
			if (v.z > maxes.z) {
				maxes.z = v.z;
			}
			if (v.x < mins.x) {
				mins.x = v.x;
			}
			if (v.y < mins.y) {
				mins.y = v.y;
			}
			if (v.z < mins.z) {
				mins.z = v.z;
			}
		}
		return maxes.ToString () + "  " + mins.ToString ();
	}

	public Vector3 recentRamps;
	public Vector3 lastAngle;

	private void TallyRamps (Vector3 toCheck)
	{
		if (Mathf.Abs (toCheck.x) > 5f) {
			if (Mathf.Sign (toCheck.x) != lastAngle.x) {
				recentRamps.x++;
				lastAngle.x = Mathf.Sign (toCheck.x);
			}
		}
		if (Mathf.Abs (toCheck.y) > 5f) {
			if (Mathf.Sign (toCheck.y) != lastAngle.y) {
				recentRamps.y++;
				lastAngle.y = Mathf.Sign (toCheck.y);
			}
		}
		if (Mathf.Abs (toCheck.z) > 5f) {
			if (Mathf.Sign (toCheck.z) != lastAngle.z) {
				recentRamps.z++;
				lastAngle.z = Mathf.Sign (toCheck.z);
			}
		}
	}

	private void ResetRamps ()
	{
		recentRamps = Vector3.zero;
		lastAngle = Vector3.zero;
	}

	public AudioSource source;
	public bool dinging = false;
	public System.Collections.Generic.Queue<float> toDing;
	
	void leanDing (int dir)
	{
		Ding (dir + 9);
	}
	
	public void Ding (int pitchMode)
	{
		toDing.Enqueue ((1f - pitchMode % 3) + (pitchMode / 9f));

		if (!dinging) {
			StartCoroutine (reallyDing ());
			dinging = true;
		}
		if (!dinged.Contains (pitchMode)) {
//			text2.text += pitchMode + "  ";
			dinged.Add (pitchMode);
		}
	}
	
	private HashSet<int> dinged;
	
	public IEnumerator reallyDing ()
	{
//		text2.text = "observed motions: ";
		while (toDing.Count > 0) {
			//text.text += toDing.Peek().ToString() + "  ";
//			source.pitch = toDing.Dequeue ();
			//source.Play();
			yield return new WaitForSeconds (.1f);
			
		}
		dinging = false;
		dinged.Clear ();
		recentRamps = Vector3.zero;
//		text2.text += " done";
	}
}
