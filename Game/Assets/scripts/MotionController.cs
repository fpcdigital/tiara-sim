﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MotionController : MonoBehaviour
{

	public bool[][] xyzmnp;
	public int[] twirlCounts;
	public int[] twirlRamps;
	public Vector3 isRamping;
	private Vector3 baseGravitZ;
	public ReadOut readout;
	public Vector3 velocity;
	public Vector3 position;

	void Awake ()
	{
//		lastAccel = Vector3.one;
//		recentChanges = new Vector3[15];
//		recentMotion = new Vector3[15];
//		lastSecond = System.DateTime.Now.Second;
//		baseVector = Input.acceleration;
		resetXYZ ();
		Input.gyro.enabled = true;
//		StartCoroutine (EvaluateHop ("left",2));
	}

	public Vector3 getAccel ()
	{
		return CorrectForCalibration (Input.acceleration - Input.gyro.gravity);
	}

	public IEnumerator EvaluateHop (string dir, int num, float[] report)
	{
		velocity = Vector3.zero;
		position = Vector3.zero;

		ResetRamps ();
		
		Vector2 movedDir = Vector2.zero;
		readout.MakeMark ();
		TallyRamps (getAccel ());
		while (lastAngle.y <= 0) {
			velocity += getAccel () * Time.deltaTime;
			position += velocity;
			if (lastAngle.x != 0 && isRamping.x != 0f) {
				movedDir.x = lastAngle.x * getAccel ().x;
			}
			if (lastAngle.z != 0 && isRamping.z != 0f) {
				movedDir.y = lastAngle.z * getAccel ().z;
			}
			yield return null;
			TallyRamps (getAccel ());
		}
//		readout.MakeMark();
		Vector3 storeAccel = Vector3.zero;
		while (lastAngle.y > -1) {
			velocity += getAccel () * Time.deltaTime;
			position += velocity;
			if (lastAngle.x != 0 && isRamping.x != 0f) {
				movedDir.x = lastAngle.x * getAccel ().x;
			}
			if (lastAngle.z != 0 && isRamping.z != 0f) {
				movedDir.y = lastAngle.z * getAccel ().z;
			}
			yield return null;
			storeAccel += getAccel ();
			TallyRamps (getAccel ());
		}
//		readout.MakeMark();
		while (lastAngle.y > -1) {
			velocity += getAccel () * Time.deltaTime;
			position += velocity;
			if (lastAngle.x != 0 && isRamping.x != 0f) {
				movedDir.x = lastAngle.x * getAccel ().x;
			}
			if (lastAngle.z != 0 && isRamping.z != 0f) {
				movedDir.y = lastAngle.z * getAccel ().z;
			}
			yield return null;
			TallyRamps (getAccel ());
		}
//		readout.MakeMark();
		Debug.Log(movedDir+" is moved dir");                                   
		if (movedDir.x > movedDir.y) {
			movedDir = new Vector2 (storeAccel.x, 0f);
		} else {
			movedDir = new Vector2 (0f, storeAccel.z);
		}
		
		Dictionary<string, Vector2> dirDict = new Dictionary<string, Vector2> (){
			{"forward",-Vector3.up},
			
			{"backward",Vector3.up},
			{"left",-Vector3.right},
			{"right",Vector3.right}};
		
//		Debug.Log (position);
				
		Debug.Log ("Unique tag! " + movedDir.ToString () + "    " + Time.time.ToString ());//this evaluates hops, can ding
		
		
		if (!(string.IsNullOrEmpty (dir) || (Vector3.Dot (position, dirDict [dir]) > 0))) {//&& Mathf.Abs(Vector3.Dot(position, dirDict[dir])) > Mathf.Abs(Vector3.Dot(position, dirDict[(dir=="left"||dir=="right"?"forward":"right")]))))) {
			
//			Debug.Log ("Fail! " + dir + "  " + movedDir + "  " + dirDict [dir]);
			report [0] = -1f;
		} else {
			for (int i = 1; i < num; i++) {
				while (lastAngle.y <= 0) {
					yield return null;
					TallyRamps (getAccel ());
				}
//				readout.MakeMark();
				report [0] = (i + .5f) / (float)num;
				while (lastAngle.y >= 0) {
					yield return null;
					TallyRamps (getAccel ());
				}
				report [0] = (i + 1f) / (float)num;
			}
			report [0] = 1;
//			Debug.Log ("successful");
		}
//		Debug.Log ("fail?");
	}
	
	public IEnumerator reportHopDir(int[] reportLean){
		
		while("the cows"!="home")
		{
			float[] leftCount = new float[1];
		StartCoroutine(EvaluateHop("left",1,leftCount));
		
			float[] fwdCount = new float[1];
		StartCoroutine(EvaluateHop("forward",1,fwdCount));
		
			float[] rightCount = new float[1];
		StartCoroutine(EvaluateHop("right",1,rightCount));
	
		bool found = false;
			while(!found){
				if(leftCount[0]==1){reportLean[0]++; found = true;}
				else if(fwdCount[0]==1){reportLean[1]++; found = true;}
				else if(rightCount[0]==1){reportLean[2]++; found = true;}
				yield return null;
			}
		}
		yield return null;
	}
	
	public IEnumerator CountHops (int[] report, float totalTime)//ignore, this is a duplicate function for a game mode not written yet.
	{
	
		float timeStamp = Time.time;
	
		velocity = Vector3.zero;
		position = Vector3.zero;
		
		ResetRamps ();
		
		Vector2 movedDir = Vector2.zero;
		readout.MakeMark ();
		TallyRamps (getAccel ());
		while (lastAngle.y <= 0 && Time.time < timeStamp + totalTime) {
			velocity += getAccel () * Time.deltaTime;
			position += velocity;
			if (lastAngle.x != 0 && isRamping.x != 0f) {
				movedDir.x = lastAngle.x * getAccel ().x;
			}
			if (lastAngle.z != 0 && isRamping.z != 0f) {
				movedDir.y = lastAngle.z * getAccel ().z;
			}
			yield return null;
			TallyRamps (getAccel ());
		}
		//		readout.MakeMark();
		Vector3 storeAccel = Vector3.zero;
		while (lastAngle.y > -1 && Time.time < timeStamp + totalTime) {
			velocity += getAccel () * Time.deltaTime;
			position += velocity;
			if (lastAngle.x != 0 && isRamping.x != 0f) {
				movedDir.x = lastAngle.x * getAccel ().x;
			}
			if (lastAngle.z != 0 && isRamping.z != 0f) {
				movedDir.y = lastAngle.z * getAccel ().z;
			}
			yield return null;
			storeAccel += getAccel ();
			TallyRamps (getAccel ());
		}
		//		readout.MakeMark();
		while (lastAngle.y > -1 && Time.time < timeStamp + totalTime) {
			velocity += getAccel () * Time.deltaTime;
			position += velocity;
			if (lastAngle.x != 0 && isRamping.x != 0f) {
				movedDir.x = lastAngle.x * getAccel ().x;
			}
			if (lastAngle.z != 0 && isRamping.z != 0f) {
				movedDir.y = lastAngle.z * getAccel ().z;
			}
			yield return null;
			TallyRamps (getAccel ());
		}
		//		readout.MakeMark();

		if (Time.time < timeStamp + totalTime) {	
			report [0] = 1;
		}
		
		while (Time.time < timeStamp + totalTime) {
			Debug.Log ("Top o the mornin  " + (timeStamp + totalTime - Time.time).ToString ());
			
			while (lastAngle.y <= 0 && Time.time < timeStamp + totalTime) {
				yield return null;
				TallyRamps (getAccel ());
			}
			float hopStamp = Time.time + 1f;
			while (lastAngle.y >= 0 && Time.time < timeStamp + totalTime && Time.time < hopStamp) {
				yield return null;
				TallyRamps (getAccel ());
			}
			if (Time.time < timeStamp + totalTime && Time.time < hopStamp) {
				report [0]++;
			}
		}
	}
		
	public IEnumerator ReportHops (int[] report){
		velocity = Vector3.zero;
		position = Vector3.zero;
		
		ResetRamps ();
		
		Vector2 movedDir = Vector2.zero;

		TallyRamps (getAccel ());
		while (lastAngle.y <= 0) {
			velocity += getAccel () * Time.deltaTime;
			position += velocity;
			if (lastAngle.x != 0 && isRamping.x != 0f) {
				movedDir.x = lastAngle.x * getAccel ().x;
			}
			if (lastAngle.z != 0 && isRamping.z != 0f) {
				movedDir.y = lastAngle.z * getAccel ().z;
			}
			yield return null;
			TallyRamps (getAccel ());
		}
		//		readout.MakeMark();
		Vector3 storeAccel = Vector3.zero;
		while (lastAngle.y > -1) {
			velocity += getAccel () * Time.deltaTime;
			position += velocity;
			if (lastAngle.x != 0 && isRamping.x != 0f) {
				movedDir.x = lastAngle.x * getAccel ().x;
			}
			if (lastAngle.z != 0 && isRamping.z != 0f) {
				movedDir.y = lastAngle.z * getAccel ().z;
			}
			yield return null;
			storeAccel += getAccel ();
			TallyRamps (getAccel ());
		}
		Debug.Log(movedDir);
		if (movedDir.x > movedDir.y) {
			movedDir = new Vector2 (storeAccel.x, 0f);
		} else {
			movedDir = new Vector2 (0f, storeAccel.z);
		}
		
//		Dictionary<Vector2, string> dirDict = new Dictionary<Vector2, string>(){
//			{-Vector3.up,"forward"},
//			{"backward",Vector3.up},
//			{"left",-Vector3.right},
//			{"right",Vector3.right}};
//		
//		Debug.Log (position);
//		
//		Debug.Log ("Unique tag! " + movedDir.ToString () + "    " + Time.time.ToString ());//this evaluates hops, can ding
//		
//		
//		if (!(string.IsNullOrEmpty (dir) || (Vector3.Dot (position, dirDict [dir]) > 0))) {//&& Mathf.Abs(Vector3.Dot(position, dirDict[dir])) > Mathf.Abs(Vector3.Dot(position, dirDict[(dir=="left"||dir=="right"?"forward":"right")]))))) {
//			
//			Debug.Log ("Fail! " + dir + "  " + movedDir + "  " + dirDict [dir]);
//			report [0] = -1f;
//		} else {
//			for (int i = 1; i < num; i++) {
//				while (lastAngle.y <= 0) {
//					yield return null;
//					TallyRamps (getAccel ());
//				}
//				//				readout.MakeMark();
//				report [0] = (i + .5f) / (float)num;
//				while (lastAngle.y >= 0) {
//					yield return null;
//					TallyRamps (getAccel ());
//				}
//				report [0] = (i + 1f) / (float)num;
//			}
//			report [0] = 1;
//			Debug.Log ("successful");
//		}
//		Debug.Log ("fail?");
	}
					
	public IEnumerator evaluateTwirl (string dir, int num, float[] report)
	{
		ResetTwirls ();
		
		
		bool noDir = dir == "none";
		var dirIndex = (dir == "left" ? 1 : 0);
		
		while ((noDir && twirlCounts[0]+twirlCounts[1]<num) || (twirlCounts[dirIndex] < num && twirlCounts[1-dirIndex]==0)) {
		
			var att = Input.gyro.attitude;
//			#if UNITY_ANDROID
//		var faceAngle = att.x+att.y+att.z+att.w;
//			#elif UNITY_IOS
			var faceAngle = Mathf.Atan2 (att.x + att.w, att.y + att.z);
//			#endif//will break on third device
			yield return null;
			checkTwirlBlock (faceAngle);
			report [0] = (float)twirlCounts [dirIndex] / (float)num;
		}
		if (!noDir && twirlCounts [1 - dirIndex] != 0) {
			report [0] = -1f;
//			Debug.Log ("FAIL");
		} else {
//			readout.MakeMark();
			report [0] = 1f;
		}
		yield return null;
	}
	
	public IEnumerator CountTwirls (int[] report)
	{
		ResetTwirls ();
		
		int[] lastTwirlCounts = new int[twirlCounts.Length];
		
		while (true) {
			
			var att = Input.gyro.attitude;
			//			#if UNITY_ANDROID
			//		var faceAngle = att.x+att.y+att.z+att.w;
			//			#elif UNITY_IOS
			var faceAngle = Mathf.Atan2 (att.x + att.w, att.y + att.z);
			//			#endif//will break on third device
			yield return null;
			checkTwirlBlock (faceAngle);
			if (lastTwirlCounts [0] != twirlCounts [0]) {
				report [0]++;
				lastTwirlCounts [0] = twirlCounts [0];
			}
			if (lastTwirlCounts [1] != twirlCounts [1]) {
				report [1]++;
				lastTwirlCounts [1] = twirlCounts [1];
			}
		}
		yield return null;
	}
	
	
	float angleTwirlRadius = .25f;
	public IEnumerator CountTwirlsAtAngle (int[] report)
	{//this will likely be superceded by dingblock once that functionality is provided for with sound.
		float FA = facingAngle;
		while ("pigs"!="fly") {
			while (Mathf.Abs(CFFA(FA)) < angleTwirlRadius) {
				yield return null;
			}
			if (CFFA (FA) >= 0) {
				report [0]++;
			} else {
				report [1]++;
			}
			while (Mathf.Abs(CFFA(FA)) > angleTwirlRadius) {
				yield return null;
			}
		}
	}
	
	//Correct For First Angle
	private float CFFA (float firstAngle)
	{//between 0 and 1, centered around firstAngle. 
		var corrected = (facingAngle - firstAngle) / 1.6f;
		corrected = (corrected + .5f) % 1f - .5f;
		return corrected;
	}
	
	public IEnumerator evaluateTwirl (DingFunc func)
	{
		ResetTwirls ();
		
		
		
		while (true) {
			
			var att = Input.gyro.attitude;
			//			#if UNITY_ANDROID
			//		var faceAngle = att.x+att.y+att.z+att.w;
			//			#elif UNITY_IOS
			var faceAngle = Mathf.Atan2 (att.x + att.w, att.y + att.z);
			//			#endif//will break on third device
			yield return null;
			DingTwirlBlock (faceAngle, func);
		}
		yield return null;
	}
	
	int lastVal = 0;
	
	public delegate void DingFunc (int toDing);
	
	private void DingTwirlBlock (float currentVal, DingFunc func)
	{
		#if UNITY_ANDROID
		int newBlock = (int)currentVal;
		#elif UNITY_IOS
		int newBlock = (int)((((currentVal/Mathf.PI)*2f)*2f+4f)%4-2f);
		#endif
		int newVal = Mathf.RoundToInt (newBlock * 10f);
		if (newVal != lastVal) {
			lastVal = newVal;
			func (newVal);
		}
	}
	
	public IEnumerator evaluateLean (string dir, float[] report)
	{
//		baseGravitZ = Input.gyro.gravity;
#if UNITY_IOS
		baseGravitZ =  Vector3.left;
#else
		baseGravitZ = Vector3.down;
		#endif
		bool complete = false;
		while (!complete) {
			Vector3 leanAngle = Input.gyro.gravity;
			if ((leanAngle - baseGravitZ).magnitude > .4f) {
				Debug.Log(leanAngle.ToString());
				if (leanAngle.z > .35f) {//fwd
					report [0] = dir.Equals ("forward") ? 1 : 0;
					complete = true;
				} else if (leanAngle.z < -.35f) {//bwd
					report [0] = dir.Equals ("backward") ? 1 : 0;
					complete = true;
				}
				if (leanAngle.x > .35f) {//r
					report [0] = dir.Equals ("right") ? 1 : 0;
					complete = true;
				} else if (leanAngle.x < -.35f) {
					report [0] = dir.Equals ("right") ? 1 : 0;
					complete = true;
				}
			}
			yield return null;
		}
		while ((Input.gyro.gravity - baseGravitZ).magnitude > .2f) {
			yield return null; 
		}
//		readout.MakeMark();
		yield return null;
	}
	
	public IEnumerator loopLean (int[] report)
	{
		while (true) {
			yield return StartCoroutine (reportLean (report));
		}
	}
	
	public IEnumerator reportLean (int[] report)
	{
		#if UNITY_IOS
		baseGravitZ = Vector3.left;
		#else
		baseGravitZ = Vector3.down;
		#endif
		bool complete = false;
		while (!complete) {
			Vector3 leanAngle = Input.gyro.gravity;
			if ((leanAngle - baseGravitZ).magnitude > .6f) {
				if (leanAngle.z > .4f) {//fwd
					report [2] = 1;
					complete = true;
				} else
				if (leanAngle.x > .4f) {//r
					report [1] = 1;
					complete = true;
				} else if (leanAngle.x < -.4f) {
					report [0] = 1;
					complete = true;
				} else {
//					Debug.Log ("No detect");
				}
			} else {
//				Debug.Log ("Not leaning");
			}
			yield return null;
		}
		float giveUpStamp = Time.time + 5f;
		while ((Input.gyro.gravity - baseGravitZ).magnitude > .6f && giveUpStamp > Time.time) {
			yield return null; 
//			Debug.Log ("Waiting for leanback");
		}
		//		readout.MakeMark();
		yield return null;
	}
	
	public IEnumerator evaluateLeanFail (string dir, float[] report)
	{
		baseGravitZ = Input.gyro.gravity;
		bool complete = false;
		while (!complete) {
			Vector3 leanAngle = Input.gyro.gravity;
			if ((leanAngle - baseGravitZ).magnitude > .5f) {
				if (leanAngle.z > .5f) {//fwd
					report [0] = dir.Equals ("forward") ? 1 : 0;
					complete = true;
				} else if (leanAngle.z < -.5f) {//bwd
					report [0] = dir.Equals ("backward") ? 1 : 0;
					complete = true;
				}
				if (leanAngle.x > .5f) {//r
					report [0] = dir.Equals ("right") ? 1 : 0;
					complete = true;
				} else if (leanAngle.x < -.5f) {
					report [0] = dir.Equals ("right") ? 1 : 0;
					complete = true;
				}
			}
			yield return null;
		}
		//		readout.MakeMark();
		yield return null;
	}
	
	//shake, nod, complete
	public IEnumerator detectNod (bool[] report)
	{
		
	
		yield return null;
		IEnumerator nodRoutine;
		IEnumerator shakeRoutine;
		var nodsNeeded = 2;
		var shakesNeeded = 2;
		
		while (nodsNeeded > 0 && shakesNeeded > 0) {
			nodRoutine = waitForNod ();
			shakeRoutine = waitForShake ();
			while (nodRoutine.MoveNext() && shakeRoutine.MoveNext()) {
				yield return null;
			}
			if (!nodRoutine.MoveNext ()) {
				nodsNeeded--;
			}
			if (!shakeRoutine.MoveNext ()) {
				shakesNeeded--;
			}
		}
		
		report [0] = shakesNeeded == 0;
		report [1] = nodsNeeded == 0;
		report [2] = true;
			
	}
	
	float nodDistance = .1f;
	
	private IEnumerator waitForNod ()
	{
		float lastVal = headAngle;
		while (headAngle < lastVal + nodDistance) {
			lastVal = Mathf.Min (lastVal, headAngle);
			yield return null;
		}
		lastVal = headAngle;
		while (headAngle > lastVal - nodDistance) {
			lastVal = Mathf.Max (lastVal, headAngle);
			yield return null;
		}
		yield return null;
	}
	
	private IEnumerator waitForShake ()
	{
		float lastVal = facingAngle;
		while (facingAngle < lastVal + nodDistance) {
			lastVal = Mathf.Min (lastVal, facingAngle);
			yield return null;
		}
		lastVal = facingAngle;
		while (facingAngle > lastVal - nodDistance) {
			lastVal = Mathf.Max (lastVal, facingAngle);
			yield return null;
		}
		yield return null;
	}
	
	public float facingAngle {
		get{ var att = Input.gyro.attitude;
			return Mathf.Atan2 (att.x + att.w, att.y + att.z);}
	}
	
	public float headAngle {
		get{ var att = Input.gyro.attitude;
			return Mathf.Atan2 (att.y - att.x, att.z - att.w);}
	}
	
	public Vector3 CorrectForCalibration (Vector3 source)
	{
		return Quaternion.FromToRotation (Input.gyro.gravity, Vector3.down) * source;		
	}

	private bool doneDancing;
	public float danceNoise;
	
	void leanDing (int dir)
	{
		Ding (dir + 9);
	}
	
	void Ding (int toDing)
	{
//		StartCoroutine (DingRoutine (toDing));
	}
	
	public float dingDelay = 0f;
	
//	IEnumerator DingRoutine (int toDing)
//	{
//		int myStep = ++waitStep;//good idea! not necessary here anymore
//		yield return new WaitForSeconds (.11f + dingDelay);
//		xyzmnp [toDing / 3] [toDing % 3] = true;
//		if (waitStep == myStep) {
//			doneDancing = true;
//			dingDelay = 0f;
//		}
//	}
	
	void resetXYZ ()
	{
		xyzmnp = new bool[][]{
			new bool[3],
			new bool[3],
			new bool[3],
			new bool[3],
			new bool[3]
		};
		ResetTwirls ();
	}
	
	void ResetTwirls ()
	{
		twirlCounts = new int[2];
		twirlRamps = new int[4];
	}

	public Vector3 recentRamps;
	public Vector3 lastAngle;

	private void TallyRamps (Vector3 toCheck)
	{
//		Debug.Log(toCheck);
		isRamping = Vector3.one;
		if (Mathf.Abs (toCheck.x) > .1f) {
			if (Mathf.Sign (toCheck.x) != lastAngle.x) {
				recentRamps.x++;
				lastAngle.x = Mathf.Sign (toCheck.x);
			}
		} else {
			isRamping.x = 0;
		}
		if (Mathf.Abs (toCheck.y) > .9f) {
			if (Mathf.Sign (toCheck.y) != lastAngle.y) {
				recentRamps.y++;
				lastAngle.y = Mathf.Sign (toCheck.y);
			}
		} else {
			isRamping.y = 0;
		}
		if (Mathf.Abs (toCheck.z) > .1f) {
			if (Mathf.Sign (toCheck.z) != lastAngle.z) {
				recentRamps.z++;
				lastAngle.z = Mathf.Sign (toCheck.z);
			}
		} else {
			isRamping.z = 0;
		}
	}
	
	private void checkTwirlBlock (float currentVal)
	{
		#if UNITY_ANDROID
		int newBlock = (int)currentVal + 1;

#elif UNITY_IOS
		int newBlock = (int)((((currentVal/Mathf.PI)*2f)*2f+4f)%4-2f);
#endif
		if (newBlock != twirlRamps [twirlRamps.Length - 1]) {
			for (int i = 1; i < twirlRamps.Length; i++) {
				twirlRamps [i - 1] = twirlRamps [i];
			}
			twirlRamps [twirlRamps.Length - 1] = newBlock;
			if (twirlRamps.SequenceEqual (new int[]{-1,0,1,-1}) ||
				twirlRamps.SequenceEqual (new int[]{0,1,-1,0}) ||
				twirlRamps.SequenceEqual (new int[]{1,-1,0,1})
			   ) {
				twirlCounts [0]++;
				twirlRamps = new int[4];
			} else if (twirlRamps.SequenceEqual (new int[]{-1,1,0,-1}) ||
				twirlRamps.SequenceEqual (new int[]{0,-1,1,0}) ||
				twirlRamps.SequenceEqual (new int[]{1,0,-1,1})) {
				twirlCounts [1]++;
				twirlRamps = new int[4];//fill with last element?
			}
		}
	}

	private void ResetRamps ()
	{
		recentRamps = Vector3.zero;
		lastAngle = Vector3.zero;
	}
	
#region conveniences
	static float sum (float[] toSum)
	{
		float result = 0f;
		for (int i = 0; i < toSum.Length; i++) {
			result += toSum [i];
		}
		return result;
	}
	
	static Vector3 sum (Vector3[] toSum)
	{
		Vector3 result = Vector3.zero;
		for (int i = 0; i < toSum.Length; i++) {
			result += toSum [i];
		}
		return result;
	}
	
	static float sum (float[] toSum, int offset, int length)
	{
		//		if(offset+length)
		float result = 0f;
		for (int i = offset; i < length; i++) {
			result += toSum [i];
		}
		return result;
	}
	
	static Vector3 sum (Vector3[] toSum, int offset, int length)
	{
		Vector3 result = Vector3.zero;
		for (int i = offset; i < length; i++) {
			result += toSum [i];
		}
		return result;
	}
#endregion
}
