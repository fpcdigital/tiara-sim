﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

partial class Gamelogic
{

	public IEnumerator FairyGodmotherSays ()
	{
		InterpretAndQueue("RS2b");
		yield return StartCoroutine(waitForSpeech());
		juke.SetBGM("background");
		IEnumerator<string> keyDropper = new KeyDropper();
		int points =  0;
		//say or do not say FG1
		//play dance move
		//success = did dance move XOR not said FG1
		//if success, points (always?)
		//if not success, read out points and restart (I assume)
		bool failure = false;
		while (!failure) {
		var doMove = false;
			if (Random.value < .5f) {
				InterpretAndQueue ("FG1");
				doMove = true;
			}
			if(points > 14){currentLevel = 2;} else if(points >9){currentLevel = 1;} else {currentLevel = 0;}
			keyDropper.MoveNext();
			string nextMove = keyDropper.Current;
			InterpretAndQueue(audioSequences[nextMove]);
			if(!doMove){
				nextMove = "none";
			}
			bool[] didMoveFlag = new bool[2];
			yield return StartCoroutine(waitForSpeech());
			yield return WaitForMove(nextMove,didMoveFlag,(doMove?15f:5f));
			if(didMoveFlag[0]){
				points++;
				InterpretAndQueue("correct+CORRECT"+Random.Range(1,5).ToString())
;			} else {
				failure = true;
				if(!doMove){
					InterpretAndQueue("incorrect+FG2");
				} else {
					InterpretAndQueue("incorrect");
					//shoulda done da move scrub!
				}
			}
		}
		juke.playBGM = false;
		InterpretAndQueue("MUSIC4+MM3+GE1+N" + points + "+GE2+REPEAT1");
	}

}
