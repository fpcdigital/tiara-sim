﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

partial class Gamelogic
{
	
	private List<string> moveTypes = new List<string>(new string[]{
		"RS4d",
		"RS4e",
		"RS4g"
	});
//

	public IEnumerator TimeToMove ()
	{
		int[] countBox = new int[1];
		int moveTypeInt = Random.Range(0,3);
	//randomly select JJs, 1-leg-hops, or skips
//		InterpretAndQueue("RS4B+RS4c+"+moveTypes[moveTypeInt]+"+RS4h+@+@+RS6");
		InterpretAndQueue("RS8a+RS8b");
		yield return StartCoroutine(waitForSpeech());
		juke.playBGM = true;
		yield return StartCoroutine (motionController.CountHops(countBox,10f));
		juke.playBGM = false;
		InterpretAndQueue("MUSIC4+MM3+GE1+N" + countBox[0] + "+GE2+REPEAT1");//only over 2 for hops/(skips?). no twirls!
		}

//	public IEnumerator CountNonXs (float timeLimit)
//	{
//		count = 0;
//		yield return StartCoroutine (motionController.waitForDance (timeLimit, new bool[1]));
//		count = (int)Mathf.Max (motionController.recentRamps.z, motionController.recentRamps.y);
//	}

}
