﻿using UnityEngine;
using System.Collections;

partial class Gamelogic {
	bool paused = false;
	float startStamp=Time.time;
	public IEnumerator DanceParty(){
		juke.SetBGM("TIARA_MUSIC1");
		
		InterpretAndQueue("RS1b+RS1c+RS1d+@+@+RS8a+RS8b");
		yield return StartCoroutine(waitForSpeech());
		juke.playBGM = true;
		juke.loopBGM = false;
		startStamp = Time.time;
		int[] pointsBox = new int[1];
		Coroutine parallelRoutine = StartCoroutine(loopNonEval(pointsBox));
		

		while(Time.time < startStamp + 16f)
		{
			bool[] didMoveFlag = new bool[2];
			yield return WaitForMove("none",didMoveFlag,5f);
			if( didMoveFlag[0]){
				pointsBox[0]--;
			}
			yield return null;
		}
		
		Gamelogic.currentLevel = 0;
		
		KeyDropper dropper = new KeyDropper();
		
		dropper.MoveNext();
		string moveKey = dropper.Current;
		paused = true;
		InterpretAndQueue("RS1e+"+audioSequences[moveKey]);
		yield return StartCoroutine(waitForSpeech());
		
		
		bool[] BonusFlag = new bool[2];
		IEnumerator BonusRoutine = WaitForMove (moveSequences[moveKey],BonusFlag,10f);
	
		while (Time.time < startStamp+26f){
			
			
			
			if(!BonusRoutine.MoveNext()){
				if(BonusFlag[0]){
					pointsBox[0]++;
				}
				BonusFlag = new bool[2];
				BonusRoutine = WaitForMove(moveSequences[moveKey],BonusFlag,10f);
				DingNow("2");
			}
			yield return null;
		}
		InterpretAndQueue("RS1c");
		paused = false;
		
		while (Time.time < startStamp+46f){
			
			
			
			yield return null;
		}
		
		dropper.MoveNext();
		moveKey = dropper.Current;
		paused = true;
		InterpretAndQueue("RS1e+"+audioSequences[moveKey]);
		yield return StartCoroutine(waitForSpeech());
		
		while (Time.time < startStamp+56f){
			
			
			
			if(!BonusRoutine.MoveNext()){
				if(BonusFlag[0]){
					pointsBox[0]++;
				}
				BonusFlag = new bool[2];
				BonusRoutine = WaitForMove(moveSequences[moveKey],BonusFlag,10f);
				DingNow("2");
			}
			
			yield return null;
		}
		InterpretAndQueue("RS1c");
		paused = false;
		
		
		while (Time.time < startStamp+62f){
			
			yield return null;
		}
		
		dropper.MoveNext();
		moveKey = dropper.Current;
		paused = true;
		InterpretAndQueue("RS1e+"+audioSequences[moveKey]);
		yield return StartCoroutine(waitForSpeech());
		
		while (Time.time < startStamp+72f){
			
			
			
			if(!BonusRoutine.MoveNext()){
				if(BonusFlag[0]){
					pointsBox[0]++;
				}
				BonusFlag = new bool[2];
				BonusRoutine = WaitForMove(moveSequences[moveKey],BonusFlag,10f);
				DingNow("2");
			}
			
			yield return null;
		}
		InterpretAndQueue("RS1c");
		paused = false;
		
		
		while (Time.time < startStamp+84f){
			
			yield return null;
		}
		
		juke.playBGM = false;
		juke.loopBGM = true;
		StopCoroutine(parallelRoutine);
		yield return StartCoroutine(sayPoints(pointsBox[0]));

//		up to sixty seconds of not-silence.
//		three occurrences of bonus opportunities
//		after five uses of the bonus opportunities "keep dancing!" and continue.

//times from email

//msfx1 every five you're dancing
//msfx2 at bonus moves

	}
	
	public IEnumerator loopNonEval(int[] pointsBox){
		while(true){
			float startTime = Time.time;
			bool[] FailFlag = new bool[2];
			IEnumerator FailRoutine = WaitForMove(new moveStruct("none"),FailFlag,5f);
			while (FailRoutine.MoveNext() || Time.time < startTime+5f){
				yield return null;
			}
			if(!paused){
				if(FailFlag[0]){
					pointsBox[0]--;
				} else {
					pointsBox[0]+=3;
					DingNow("1");
				}
			}
		}
	}
	
	void DingNow(string type){
	string suffix = "a";
		if(Time.time - startStamp > 36){
			suffix = "b";
			if(Time.time - startStamp > 54){
				suffix = "c";
			}
		}
		juke.playOneShot("MSFX"+type+suffix);
	}
	
}
