﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

partial class Gamelogic
{
	
	public IEnumerator RLGL ()
	{
		juke.SetBGM ("BGMusicInstrument");
		InterpretAndQueue("RS3b+RS3c+@+@+RS8a+RS8b");
		yield return StartCoroutine(waitForSpeech());
		bool done = false;
		int points = 0;
		List<Coroutine> toStop = new List<Coroutine>();
//		while (!done) {
			juke.playBGM = true;
			int[] hopCounts = new int[3];
		toStop.Add(StartCoroutine (motionController.reportHopDir (hopCounts)));
			int[] lastCounts = new int[3];
			int[] leanReport = new int[3];
		toStop.Add(StartCoroutine (motionController.loopLean (leanReport)));
			//		motionController.evaluateTwirl((int x)=>) // use this when you have single harp hit
			int[] twirlReport = new int[2];
		toStop.Add(StartCoroutine (motionController.CountTwirlsAtAngle (twirlReport)));
		
			float endStamp = Time.time + 30f;
		
		
		
			while (Time.time < endStamp) {
				if (hopCounts [0] != lastCounts [0]) {
					juke.playOneShot ("SKIP1_Violin1");
					points++;
				}
				if (hopCounts [1] != lastCounts [1]) {
					juke.playOneShot ("JUMP_UP_Crash");
					points++;
				}
				if (hopCounts [2] != lastCounts [2]) {
					juke.playOneShot ("SKIP2_Violin2");
					points++;
				}
				hopCounts.CopyTo (lastCounts, 0);
			
				if (leanReport [0] == 1) {
					juke.playOneShot ("LEAN_LEFT_Glock1");
					leanReport [0] = 0;
					points++;
				}
			
				if (leanReport [1] == 1) {
					juke.playOneShot ("LEAN_RIGHT_Glock1");
					leanReport [1] = 0;
					points++;
				}
				if (leanReport [2] == 1) {
					juke.playOneShot ("BOW_Glock3");
					leanReport [2] = 0;
					points++;
				}
			
				if (twirlReport [0] >= 1) {
					juke.playOneShot ("TWIRL_Harp");
					twirlReport [0] = 0;
					points++;
				}
			
				if (twirlReport [1] >= 1) {
					juke.playOneShot ("TWIRL_Harp");
					twirlReport [1] = 0;
					points++;
				}
				yield return null;
			}
		juke.PlayOutput("TAGEND1");
//			juke.playOneShot ("failBuzzer");
			juke.playBGM = false;
//			bool[] didMoveFlag = new bool[2];
//			yield return WaitForMove ("none", didMoveFlag, (5f));
//			done = !didMoveFlag [0];
//		}
		foreach(Coroutine stopp in toStop){
			StopCoroutine(stopp);
		}
		juke.BGsource.clip = Resources.Load<AudioClip> ("background");
		
		yield return StartCoroutine (sayPoints (points));
	}
	
}
