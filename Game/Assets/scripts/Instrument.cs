﻿using UnityEngine;
using System.Collections;

partial class Gamelogic {

public IEnumerator Instrument(){
		juke.SetBGM("BGMusicInstrument");
		juke.playBGM = true;
		int[] hopCounts = new int[3];
		StartCoroutine (motionController.reportHopDir (hopCounts));
		int[] lastCounts = new int[3];
		int[] leanReport = new int[3];
		StartCoroutine(motionController.loopLean(leanReport));
//		motionController.evaluateTwirl((int x)=>) // use this when you have single harp hit
		int[] twirlReport = new int[2];
		StartCoroutine(motionController.CountTwirlsAtAngle(twirlReport));
		while (true){
			if (hopCounts [0] != lastCounts [0]) {
				juke.playOneShot ("SKIP1_Violin1");
		
			}
			if (hopCounts [1] != lastCounts [1]) {
				juke.playOneShot ("JUMP_UP_Crash");
	
			}
			if (hopCounts [2] != lastCounts [2]) {
				juke.playOneShot ("SKIP2_Violin2");
			
			}
			hopCounts.CopyTo (lastCounts, 0);
			
			if(leanReport[0]==1){
				juke.playOneShot("LEAN_LEFT_Glock1");
				leanReport[0]=0;
			}
			
			if(leanReport[1]==1){
				juke.playOneShot("LEAN_RIGHT_Glock1");
				leanReport[1]=0;
			}
			if(leanReport[2]==1){
				juke.playOneShot("BOW_Glock3");
				leanReport[2]=0;
			}
			
			if(twirlReport[0]>=1){
				juke.playOneShot("TWIRL_Harp");
				twirlReport[0]=0;
			}
			
			if(twirlReport[1]>=1){
				juke.playOneShot("TWIRL_Harp");
				twirlReport[1]=0;
			}
			yield return null;
		}
	
	}

}
