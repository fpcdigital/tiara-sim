﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KeyDropper : IEnumerator<string> {
	private string _current;
	public string Current{
		get{
			return _current;
		}
	}
	
	object IEnumerator.Current{
		get{return Current;}
	}

	int lastLevel = -1;
	IEnumerator<string> dropEnumerator;
	
	
	public bool MoveNext(){
		if(lastLevel != Gamelogic.currentLevel){
			dropEnumerator = Gamelogic.RandomKeys (Gamelogic.audioSequences).GetEnumerator();
			lastLevel = Gamelogic.currentLevel;
		}
		dropEnumerator.MoveNext();
		_current = dropEnumerator.Current;
		return true;
	}
	
	public void Reset(){
		lastLevel = -1;
	}
	
	public void Dispose(){
	}
}
